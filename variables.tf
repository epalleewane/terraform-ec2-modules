variable "environment" {
description= "type of environment , prod or stg or dev "
default="stg"
}
variable "instance_type" {
  description = "instance type ..."
  default = "t2.micro"
}
variable "http_port" {
    default = 80
}
variable "ssh_port" {
    default = 22
}
variable "region" {
    default = "us-east-1"
}