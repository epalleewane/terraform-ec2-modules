resource "aws_instance" "node2" {
  ami             = "ami-2757f631"
  instance_type   = var.instance_type
  key_name        = "puppetkey"
  security_groups = ["${aws_security_group.webserver_sg.name}"]
  tags = {
    Name     = "puppet_mester"
    Timeflag = "ondemand"
    apptype  = "Activedirectory"
  }
}
