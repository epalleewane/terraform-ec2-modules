resource "aws_instance" "node1" {
  ami             = "ami-2757f631"
  instance_type   = var.instance_type
  key_name        = "puppetkey"
  security_groups = ["${aws_security_group.webserver_sg.name}"]
  tags = {
    Name     = "ansible_master"
    Timeflag = "ondemand"
    apptype  = "Activedirectory"
  }
}
