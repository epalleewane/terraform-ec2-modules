terraform {
  backend "s3" {
    bucket = "remotestatefile-terraform"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}